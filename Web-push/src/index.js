require('dotenv').config(); //Asigna las variables de .env en el sistema operativo para poder acceder a ellas

const express = require('express') //Para configurar un servidor
const morgan = require('morgan') //Ver las peticiones que van llegando a nuestro servidor
const path = require('path')

const app = express()

//Configurar funciones que procesan rutas
app.use(morgan('dev'))
app.use(express.urlencoded({extended:false})) //Convertir el formulario para poder entenderlo
app.use(express.json())

//Rutas del servidor
app.use(require('./routes/index'));

//Contenido estatico -> Página HTML
app.use(express.static(path.join(__dirname, 'public')));

app.listen(3000)
console.log("Servidor escuchando...")