const PUBLIC_VAPID_KEY='BExMxmonsFFnKCtIivA77PvZfwMBnkaQzVedRR03hkgpTs_tIqoL3w-tCC7bTtExFS5QiQKcfbLqq8GefLa7Fjc';


function urlBase64ToUint8Array(base64String) {
    const padding = "=".repeat((4 - (base64String.length % 4)) % 4);
    const base64 = (base64String + padding).replace(/-/g, "+").replace(/_/g, "/");
  
    const rawData = window.atob(base64);
    const outputArray = new Uint8Array(rawData.length);
  
    for (let i = 0; i < rawData.length; ++i) {
      outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
  }
  
//Subscribir al usuario a traves de una peticion
const subscription = async() => {

    //Service Worker
    const register = await navigator.serviceWorker.register("./serviceworker.js", {
        scope: "/"
    });
    console.log("New Service Worker");

    const subscription = await register.pushManager.subscribe({
        userVisibleOnly: true,
        applicationServerKey: urlBase64ToUint8Array(PUBLIC_VAPID_KEY)
    });
    console.log(subscription);
    
    await fetch('/subscription', {
        method : 'POST',
        body: JSON.stringify(subscription),
        headers: {
            "Content-Type": "application/json"
        }
    });

    console.log("Subscrito!")

    
}


subscription();